#include <input.h>

int menu_toggle;

static void mouse_button_handler(SDL_MouseButtonEvent *e)
{
    if (curr_task == SEGMENT_STATE)
    {
        add_point(e->x, e->y);
        redraw();
    }
}

static void mouse_motion_handler(SDL_MouseMotionEvent *e)
{
    if (curr_task == SEGMENT_STATE && drawing_line())
    {
        update_motion_point(e->x, e->y);
        redraw();
    }
}

static void key_handler(SDL_KeyboardEvent *e)
{
    switch (e->keysym.sym)
    {
    /* Display help menu */
    case SDLK_h:
        menu_toggle = 1 - menu_toggle;
        redraw();
        break;

    /* Initiate segment drawing mode */
    case SDLK_l:
        menu_toggle = 0;
        curr_task != SEGMENT_STATE ?
            (curr_task = SEGMENT_STATE) : (curr_task = INIT_STATE);
		redraw();
        break;

    /* Close application when pressing ESC or q. */
    case SDLK_ESCAPE:
    case SDLK_q:
		close_app();
        break;
    }
}

void input_callbacks_init(void)
{
    callbacks.mouse_button_func = mouse_button_handler;
    callbacks.mouse_motion_func = mouse_motion_handler;
    callbacks.keyboard_func = key_handler;
}
