#ifndef _UTILS_H_
#define _UTILS_H_

#include <config.h>

#define MAX(x, y)   (x > y ? x : y)
#define MIN(x, y)   (x < y ? x : y)

float get_distance(int x1, int y1, int x2, int y2);

float get_angle_lines(int x1, int y1, int x2, int y2,
        int x3, int y3, int x4, int y4);

void debug_show_text_bounds(int x1, int y1, int x2, int y2, float text_width);

float get_rendered_text_width(char *text, TTF_Font *font, SDL_Color color);

void simplification(int *a, int *b, int *c);

#endif
