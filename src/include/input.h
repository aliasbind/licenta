#ifndef _INPUT_H_
#define _INPUT_H_

#include <config.h>

extern int menu_toggle;

/* Initializes mouse and keyboard callbacks. */
void input_callbacks_init(void);

#endif
