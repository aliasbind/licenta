#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdio.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#define DEFAULT_WIDTH   800
#define DEFAULT_HEIGHT  450

#define INIT_STATE  0
#define SEGMENT_STATE 1

struct
{
    void (*display_func)(void);
    void (*keyboard_func)(SDL_KeyboardEvent *e);
    void (*mouse_button_func)(SDL_MouseButtonEvent *e);
    void (*mouse_motion_func)(SDL_MouseMotionEvent *e);
} callbacks;

/* Main Window pointer */
extern SDL_Surface *surface;

/* Initiates a redrawing of the window */
void redraw(void);

/* Closes the current application */
void close_app(void);

/* Current task being preformed */
extern int curr_task;

#endif
