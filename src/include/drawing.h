#ifndef _DRAWING_H_
#define _DRAWING_H_

#include <config.h>
#include <input.h>
#include <utils.h>

#include <segments.h>

/* Main Window size */
extern int width, height;

/* Initialize drawing area. */
void drawing_panel_init(void);

/* Add point caught from click event */
void add_point(int x, int y);

/* 
 * Function used to render text on OpenGL viewport.
 * Basically, links SDL_ttf with OpenGL.
 */
void put_text(TTF_Font *font, SDL_Color color, char *text, float x, float y);

#endif
