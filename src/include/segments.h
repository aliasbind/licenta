#ifndef _SEGMENTS_H_
#define _SEGMENTS_H_

#include <config.h>
#include <drawing.h>

#define COORD_DIVISOR   1

/* When called, repaints viewport with added segments */
void segment_drawing_handler(void);

/* Adds point to points array. Each two consecutive points form a line. */
void add_point(int x, int y);

/* Updates the end-point of the line currently being drawn */
void update_motion_point(int x, int y);

/* If true, there is currently a line being drawn by the user */
int drawing_line(void);

#endif
