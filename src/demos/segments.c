#include <segments.h>

/* Arrays for storing lines' begin and end points. */
static int *points_x;
static int *points_y;
static int num_points;

/* Used to animate the drawing of the second point of the line. */
static int motion_x;
static int motion_y;

/* Specifies if we are currently drawing a line or not. */
static int drawing_underway;

static void put_text_above_line(char *text, int x1, int y1, int x2, int y2)
{
    TTF_Font *font;
    int font_size;
    SDL_Color color = {255, 255, 255};

    float angle;
    float text_len_pix;
    float start_coef;
    float distance;

    int text_start_x, text_start_y;

    int x_l, y_l, x_r, y_r;

    /* FIXME: font size should not be constant! */
    font_size = 24;

    /* Determine text width in pixels */
    text_len_pix = strlen(text) * (font_size / 2);
    
    /* Determine distance of the line */
    distance = get_distance(x1, y1, x2, y2);

    /* Locate start point (in %) of the line where text begins */
    start_coef = (1 - (text_len_pix / distance)) / 2;

    if (x1 < x2)
    {
        x_l = x1; y_l = y1;
        x_r = x2; y_r = y2;
    }
    else
    {
        x_l = x2; y_l = y2;
        x_r = x1; y_r = y1;
    }

    text_start_x = x_l * (1 - start_coef) + x_r * start_coef;
    text_start_y = y_l * (1 - start_coef) + y_r * start_coef;

    angle = get_angle_lines(x_l, y_l, x_r, y_r, 0, height, width, height);

    if (y_l > y_r)
        angle = -angle;

    /* Translate to point and finally rotate. */
    glPushMatrix();
    glTranslatef(text_start_x, text_start_y, 0);
    glRotatef(angle, 0, 0, 1);

    font = TTF_OpenFont("./fonts/UbuntuMono-B.ttf", font_size);
    if (!font)
    {
        printf("put_text_above_line: %s\n", TTF_GetError());
        return;
    }
    put_text(font, color, text, 0, -font_size*1.13);

    TTF_CloseFont(font);
    glPopMatrix();
}

static void print_equations(void)
{
    int i, bytes_written;
    int a, b, c;
    int p1_x, p1_y, p2_x, p2_y;
    char buf[20];

    for (i = 0; i < num_points - 1; i += 2)
    {
        bytes_written = 0;
        p1_x = points_x[i];
        p1_y = points_y[i];
        p2_x = points_x[i+1];
        p2_y = points_y[i+1];

        if (points_x[i] != points_x[i+1])
        {
            a = (p1_y / COORD_DIVISOR) - (p2_y / COORD_DIVISOR);
            b = (p1_x / COORD_DIVISOR) - (p2_x / COORD_DIVISOR);
            c = ((-p1_y) / COORD_DIVISOR) * ((p2_x / COORD_DIVISOR) - (p1_x / COORD_DIVISOR)) -
                ((-p1_x) / COORD_DIVISOR) * ((p2_y / COORD_DIVISOR) - (p1_y / COORD_DIVISOR));
        }

        simplification(&a, &b, &c);

        if (a == 1)
            bytes_written = sprintf(&buf[0], "x");
        else if (a == -1)
            bytes_written = sprintf(&buf[0], "-x");
        else if (a != 0)
            bytes_written = sprintf(&buf[0], "%dx", a);
        
        if (b == -1)
            bytes_written += sprintf(&buf[bytes_written], "-y");
        else if (b == 1)
            bytes_written += sprintf(&buf[bytes_written], "+y");
        else if (b < 0)
            bytes_written += sprintf(&buf[bytes_written], "%dy", b);
        else if (b > 0)
            bytes_written += sprintf(&buf[bytes_written], "+%dy", b);

        if (c < 0)
            bytes_written += sprintf(&buf[bytes_written], "%d", c);
        else if (c > 0)
            bytes_written += sprintf(&buf[bytes_written], "+%d", c);

        bytes_written += sprintf(&buf[bytes_written], "=0");

        put_text_above_line(&buf[0], p1_x, p1_y, p2_x, p2_y);
    }
}

void segment_drawing_handler(void)
{
    int i;

    if (!num_points)
        return;
 
    glColor3f(0.82, 0.83, 0.21);

    glLineWidth(5);
    glBegin(GL_LINES);

    for (i = 0; i < num_points; i++)
        glVertex2i(points_x[i], points_y[i]);

    if (drawing_underway)
        glVertex2i(motion_x, motion_y);
    else
        printf("last line: (%d, %d) | (%d, %d)\n",
                points_x[num_points-2], points_y[num_points-2],
                points_x[num_points-1], points_y[num_points-1]);
    glEnd();

    print_equations();
}

void add_point(int x, int y)
{
    if (!points_x || !points_y)
    {
        points_x = malloc(sizeof(int) * 256);
        points_y = malloc(sizeof(int) * 256);
    }

    if (num_points && (num_points / 256) > ((num_points - 1) / 256))
    {
        points_x = realloc(points_x, sizeof(int) * 256 * (num_points / 256 + 1));
        points_y = realloc(points_y, sizeof(int) * 256 * (num_points / 256 + 1));
    }

    points_x[num_points] = x;
    points_y[num_points] = y;
    num_points++;

    if (num_points % 2)
    {
        drawing_underway = 1;
        motion_x = x;
        motion_y = y;
    }
    else
        drawing_underway = 0;
}

void update_motion_point(int x, int y)
{
    motion_x = x;
    motion_y = y;
}

int drawing_line(void)
{
    return drawing_underway;
}
