#include <utils.h>

float get_distance(int x1, int y1, int x2, int y2)
{
    return sqrtf((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

float get_angle_lines(int x1, int y1, int x2, int y2,
        int x3, int y3, int x4, int y4)
{
    int vec1_x = x2 - x1;
    int vec1_y = y2 - y1;
    int vec2_x = x4 - x3;
    int vec2_y = y4 - y3;

    float dot_product = vec1_x * vec2_x + vec1_y * vec2_y;
    float len_vec1 = sqrtf(vec1_x * vec1_x + vec1_y * vec1_y);
    float len_vec2 = sqrtf(vec2_x * vec2_x + vec2_y * vec2_y);

    if ((vec1_x == 0 && vec1_y == 0) || (vec2_x == 0 && vec2_y == 0))
        return 0;

    return acosf(dot_product / (len_vec1 * len_vec2)) * 180 / M_PI;
}

void debug_show_text_bounds(int x1, int y1, int x2, int y2, float text_width)
{
    float dist = get_distance(x1, y1, x2, y2);
    float text_width_pct = text_width / dist;
    float begin_pct = (1.0f - text_width_pct) / 2;
    float end_pct = begin_pct + text_width_pct;

    int begin_x = (1.0f - begin_pct) * x1 + begin_pct * x2;
    int begin_y = (1.0f - begin_pct) * y1 + begin_pct * y2;

    int end_x   = (1.0f - end_pct) * x1 + end_pct * x2;
    int end_y   = (1.0f - end_pct) * y1 + end_pct * y2;

    glColor3f(1, 0, 0);
    glBegin(GL_POINTS);
    glVertex2i(begin_x, begin_y);
    glVertex2i(end_x, end_y);
    glEnd();

    glColor3f(0, 0, 1);
    glBegin(GL_POINTS);
    glVertex2i(begin_x, begin_y);
    glVertex2i(begin_x+text_width, begin_y);
    glEnd();
}

float get_rendered_text_width(char *text, TTF_Font *font, SDL_Color color)
{
    float width;
    SDL_Surface *surface;
    
    surface = TTF_RenderText_Blended(font, text, color);
    width = surface->w;
    return width;
}

int gcd(int a, int b)
{
    int i;

    if (!a)
        return b;

    if (!b)
        return a;

    for (i = MIN(a, b); i >= 1; i--)
        if ((a % i == 0) && (b % i == 0))
            return i;

    return 1;
}

void simplification(int *a, int *b, int *c)
{
    /* Get greatest common divisor for a, b and c. */
    int current_divisor = gcd(gcd(abs(*a), abs(*b)), abs(*c));

    if (current_divisor <= 1)
        return;

    *a /= current_divisor;
    *b /= current_divisor;
    *c /= current_divisor;

    simplification(a, b, c);
}
