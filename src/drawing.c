#include <drawing.h>

int width = DEFAULT_WIDTH;
int height = DEFAULT_HEIGHT;
int curr_task = INIT_STATE;

void put_text(TTF_Font *font, SDL_Color color, char *text, float x, float y)
{
    unsigned int texture;
    SDL_Surface *surface_text;

    surface_text = TTF_RenderText_Blended(font, text, color);
    if (!surface_text)
    {
        printf("put_text: %s\n", TTF_GetError());
        return;
    }

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glColor3f(1, 1, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface_text->w, surface_text->h,
            0, GL_BGRA, GL_UNSIGNED_BYTE, surface_text->pixels);

    glBegin(GL_QUADS);
    glTexCoord2d(0, 0); glVertex2f(x, y);
    glTexCoord2d(1, 0); glVertex2f(x+surface_text->w, y);
    glTexCoord2d(1, 1); glVertex2f(x+surface_text->w, y+surface_text->h);
    glTexCoord2d(0, 1); glVertex2f(x, y+surface_text->h);
    glEnd();

    glColor3f(1, 1, 1);
    glDeleteTextures(1, &texture);
    SDL_FreeSurface(surface_text);
}

static void draw_help_menu(void)
{
    const int key_indent = 150;
    const int desc_indent = 360;
    int current_row_pos = 80;
    SDL_Color key_color = {240, 127, 63};
    SDL_Color desc_color = {63, 191, 200};

    TTF_Font *desc_font = TTF_OpenFont("./fonts/UbuntuMono-R.ttf", 24);
    TTF_Font *key_font = TTF_OpenFont("./fonts/UbuntuMono-B.ttf", 24);

    if (!desc_font)
    {
        printf("desc_font: %s\n", TTF_GetError());
        return;
    }

    if (!key_font)
    {
        printf("key_font: %s\n", TTF_GetError());
        return;
    }

    put_text(key_font, key_color, "h", key_indent, current_row_pos);
    put_text(desc_font, desc_color, "Display this help menu", desc_indent, current_row_pos);
    current_row_pos += 30;
    put_text(key_font, key_color, "ESC, q", key_indent, current_row_pos);
    put_text(desc_font, desc_color, "Quit", desc_indent, current_row_pos);
    current_row_pos += 50;
    put_text(key_font, key_color, "l", key_indent, current_row_pos);
    put_text(desc_font, desc_color, "Segment drawing mode", desc_indent, current_row_pos);

    TTF_CloseFont(desc_font);
    TTF_CloseFont(key_font);
}

static void draw(void)
{
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    if (curr_task == SEGMENT_STATE)
    {
        glClearColor(0, 0.4, 0.2, 0.6);
        glClear(GL_COLOR_BUFFER_BIT);
        segment_drawing_handler();
    }

    if (menu_toggle)
    {
        glColor4f(0, 0, 0, 0.90);
        glRecti(width / 10, height / 10, width - width / 10, height - height / 10);
        draw_help_menu();
    }

    SDL_GL_SwapBuffers();
}

void drawing_panel_init(void)
{
    /* Enable SDL_ttf */
    TTF_Init();

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POINT_SMOOTH);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    gluOrtho2D(0, width, height, 0);

    callbacks.display_func = draw;
}
