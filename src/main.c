#include <config.h>
#include <input.h>
#include <drawing.h>

SDL_Surface *surface;

static int _redraw = 1;
static int _close;

void redraw(void)
{
	_redraw = 1;
}

void close_app(void)
{
	_close = 1;
}

static void display_window_init(void)
{
    surface = SDL_SetVideoMode(width, height, 16, SDL_OPENGL | SDL_RESIZABLE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
}

static void main_loop(void)
{
    SDL_Event e;

    while (1)
    {
        while (SDL_PollEvent(&e))
        {
            switch (e.type)
            {
            case SDL_QUIT:
                _close = 1;
                break;
            case SDL_KEYDOWN:
                if (callbacks.keyboard_func)
                    callbacks.keyboard_func(&e.key);
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (callbacks.mouse_button_func)
                    callbacks.mouse_button_func(&e.button);
                break;
            case SDL_MOUSEMOTION:
                if (callbacks.mouse_motion_func)
                    callbacks.mouse_motion_func(&e.motion);
                break;
            }
        }

        if (_redraw && callbacks.display_func)
        {
            callbacks.display_func();
            _redraw = 0;
        }

        if (_close)
            break;
    }
}

int main(int argc, char *argv[])
{
    /* Initialize SDL. */
    SDL_Init(SDL_INIT_VIDEO);

    /* Initialize main window. */
    display_window_init();

    /* Initialize drawing area. */
    drawing_panel_init();

    /* Initialize mouse and keyboard callback functions. */
    input_callbacks_init();

    /* Enter program main loop. */
    main_loop();

    return 0;
}
